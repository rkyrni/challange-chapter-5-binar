window.addEventListener("DOMContentLoaded", () => {
  // game Choice transform function
  const gameChoiceTransform = () => {
    const gameChoiceContentEl = document.querySelector("#game-choice-content");
    const gameChoiceCarouselEl = document.querySelector(
      "#game-choice-carousel"
    );

    if (window.scrollY > 450 && window.scrollY < 908) {
      gameChoiceContentEl.classList.remove("hidden-left");
      gameChoiceCarouselEl.classList.remove("hidden-right");
    } else {
      gameChoiceContentEl.classList.add("hidden-left");
      gameChoiceCarouselEl.classList.add("hidden-right");
    }
  };

  //   transform the Game Choice
  gameChoiceTransform();

  // transform the game choice when page is scrolled
  document.addEventListener("scroll", gameChoiceTransform);

  //   console.log(window.scrollY);
});
