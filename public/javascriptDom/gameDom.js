const playerElements = document.querySelectorAll(".player");
// console.log(playerElement);

const comElements = {
  batu: document.querySelector(".com-batu"),
  kertas: document.querySelector(".com-kertas"),
  gunting: document.querySelector(".com-gunting"),
};

const textVsElement = document.querySelector(".vs");
const resultElement = document.querySelector(".result");
const textResultElement = document.querySelector(".text-result");
const refreshElement = document.querySelector(".refresh");

let counterClick = true;
const batu = "batu";
const kertas = "kertas";
const gunting = "gunting";
const draw = "DRAW";
const playerWin = "PLAYER 1 WIN";
const comWin = "COM WIN";
const hiddenClass = "hidden";
const playerChoiceClass = "player-choose";

// ====== PLAYER CLICK ======
playerElements.forEach((element) => {
  element.addEventListener("click", () => {
    if (counterClick) {
      let interval;
      const player = element.getAttribute("data-player");
      const com = getRandomNumberInt(0, 2);

      const rollGame = new RollGameInterface(player, com);
      rollGame.playerChoice();

      element.classList.add(playerChoiceClass);
      counterClick = false;

      if (!interval) interval = setInterval(drawingColor, 50);

      setTimeout(() => {
        clearInterval(interval);
        stopInterval();
        rollGame.resultUserInterface();

        // === BUTTON REFRESH ===
        refreshElement.addEventListener("click", () => {
          rollGame.refreshUserInterface();
        });
        // === END BUTTON REFRESH ===
      }, 2000);
    }
  });
});
// ====== END PLAYER CLICK ======

// ================== FUNCTION ====================

const getRandomNumberInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

const drawingColor = () => {
  const randomColor = () => {
    return `rgb(${getRandomNumberInt(0, 255)}, ${getRandomNumberInt(
      0,
      255
    )}, ${getRandomNumberInt(0, 255)})`;
  };

  let index = 1;
  let randomNumber = getRandomNumberInt(1, 3);

  for (const element in comElements) {
    if (index === randomNumber) {
      comElements[element].style.backgroundColor = randomColor();
    } else {
      comElements[element].style.backgroundColor = "transparent";
    }
    index++;
  }
};

const stopInterval = () => {
  for (const element in comElements) {
    comElements[element].style.backgroundColor = "transparent";
  }
};

// ================== END FUNCTION ====================

// ============== CLASSES ====================
class Game {
  constructor(player, com) {
    const comSelect = [batu, kertas, gunting];

    this._player = player;
    this._com = comSelect[com];
    this._result = "";
  }

  result() {
    if (this._player === this._com) this._result = draw;
    else if (this._player === batu) {
      if (this._com === gunting) this._result = playerWin;
      else this._result = comWin;
    } else if (this._player === kertas) {
      if (this._com === batu) this._result = playerWin;
      else this._result = comWin;
    } else if ((this._player = gunting)) {
      if (this._com === kertas) this._result = playerWin;
      else this._result = comWin;
    }
    console.log(`> Player input "${this._player}"`);
    console.log(`> Com input "${this._com}"`);
    console.log(`=> Then the Result is "${this._result}"`);
  }
}

class RollGameInterface extends Game {
  constructor(player, com) {
    super(player, com);
    this.result();
  }

  #comResultElement() {
    comElements[this._com].classList.add(playerChoiceClass);
  }

  #hiddenElement() {
    textVsElement.classList.add(hiddenClass);
    resultElement.classList.remove(hiddenClass);
    refreshElement.classList.remove(hiddenClass);
  }

  #viewTextElement() {
    const lightGreen = "#4c9654";
    const darkGreen = "#035B0C";
    textResultElement.textContent = this._result;

    this._result === draw
      ? (resultElement.style.backgroundColor = darkGreen)
      : (resultElement.style.backgroundColor = lightGreen);
  }

  playerChoice() {
    playerElements.forEach((el) => {
      el.classList.remove("icon-hover", "content-hover");
    });
  }

  resultUserInterface() {
    this.#comResultElement();
    this.#hiddenElement();
    this.#viewTextElement();
  }

  refreshUserInterface() {
    for (const element in comElements) {
      comElements[element].classList.remove(playerChoiceClass);
    }

    playerElements.forEach((el) => {
      el.classList.add("icon-hover", "content-hover");
      el.classList.remove(playerChoiceClass);
    });

    refreshElement.classList.add(hiddenClass);
    resultElement.classList.add(hiddenClass);
    textVsElement.classList.remove(hiddenClass);
    counterClick = true;
  }
}
