const fs = require("fs");
const readData = require("../../utils/readData");

exports.login = (req, res) => {
  const users = readData();
  const userLogin = req.body;

  const user = users.find(
    (data) => data.email.toLowerCase() === userLogin.email.toLowerCase()
  );
  if (!user) {
    res.status(404).send("Fatal, User tidak ditemukan!!");
  } else {
    if (user.password !== userLogin.password) {
      res.status(401).send("Password tidak valid!!");
    } else res.send(user);
  }
};

exports.create = (req, res) => {
  const users = readData();
  const newUser = req.body;

  if (!newUser.name) res.status(402).send("Masukkan nama anda dengan benar");
  else if (!newUser.email)
    res.status(402).send("Masukkan email anda dengan benar");
  else if (!newUser.password)
    res.status(402).send("Masukkan password anda dengan benar");
  else {
    const duplikatEmail = users.find((data) => data.email === newUser.email);

    if (duplikatEmail) res.status(402).send("Email sudah digunakan");
    else {
      users.push(newUser);
      try {
        fs.writeFileSync("./dataUsers/users.json", JSON.stringify(users));
        res.send([newUser]);
      } catch (err) {
        res.status(501);
        err.msg = "Fatal, Mungkin terjadi kesalahan internal";
        res.send(err);
      }
    }
  }
};

exports.loginInPage = async (req, res) => {
  const users = await readData();
  const userLogin = req.body;
  const userChecked = users.filter((dataUser) => {
    return (
      dataUser.email.toLowerCase() === userLogin.email.toLowerCase() &&
      dataUser.password === userLogin.password
    );
  });
  if (userChecked.length === 0) {
    console.log("Email dan Password tidak valid");
    res.render("pages/login", {
      statusLogin: false,
    });
  } else {
    res.redirect(`/home/${userChecked[0].name}`);
  }
};

exports.createInPage = async (req, res) => {
  const users = await readData();
  const newUser = req.body;
  users.push(newUser);
  try {
    fs.writeFileSync("./dataUsers/users.json", JSON.stringify(users));
    console.log("Berhasil Daftar");
    res.redirect("/login");
  } catch {
    console.log("Gagal Daftar");
  }
};
