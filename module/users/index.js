const readData = require("../../utils/readData");

exports.users = (req, res) => {
  const users = readData();
  res.send(users);
};
