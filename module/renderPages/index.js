const readData = require("../../utils/readData");

exports.home = (req, res) => {
  res.render("pages/landing", {
    user: "",
  });
};

exports.login = (req, res) => {
  res.render("pages/login", {
    statusLogin: true,
  });
};

exports.homeUser = async (req, res) => {
  const users = await readData();
  const user = users.filter((dataUser) => {
    return dataUser.name === req.params.name;
  });

  if (user.length === 0) {
    res.status(404);
    res.render("pages/404");
  } else {
    res.render("pages/landing", {
      user,
    });
  }
};

exports.form = (req, res) => res.render("pages/form");

exports.game = (req, res) => res.render("pages/game");

exports.notFound = (req, res) => {
  res.status(404);
  res.render("pages/404");
};
