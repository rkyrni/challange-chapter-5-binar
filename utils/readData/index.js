const fs = require("fs");

const readData = () => {
  try {
    const users = fs.readFileSync("./data/users.json");
    return JSON.parse(users);
  } catch {
    console.log("GAGAL AMBIL DATA USER");
  }
};
module.exports = readData;
