const express = require("express");
const bodyParser = require("body-parser");
const usersModule = require("./module/users");
const authModule = require("./module/authentication");
const renderPagesModule = require("./module/renderPages");

const app = express();
const port = 3000;

app.set("view engine", "ejs");
app.use(bodyParser.json());
app.use(express.static("public")); //built-in middleware
app.use(express.urlencoded({ extended: true }));

app.get("/", renderPagesModule.home);

app.get("/home/:name", renderPagesModule.homeUser);

app.get("/game", renderPagesModule.game);

app.get("/form", renderPagesModule.form);

app.post("/form", authModule.createInPage);

app.get("/login", renderPagesModule.login);

app.post("/login", authModule.loginInPage);

// ENDPOINT UNTUK DI HIT DI POSTMAN
app.get("/users", usersModule.users);

app.post("/masuk", authModule.login);

app.post("/create", authModule.create);
// END

app.use(renderPagesModule.notFound);

app.listen(port, () => {
  console.log(`Listening http://127.0.0.1:${port}`);
});
